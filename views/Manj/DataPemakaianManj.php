
 <div class="row">
          <div class="col-lg-12">
            <h1> Data <small>Pemakaian Barang </small></h1>
            <ol class="breadcrumb">
                 <a href="?page=dashboard"><i class="fa fa-fw fa-dashboard"></i>Dashboard</a></li> 
                <li><i class="fa fa-shopping-cart fa"></i> Pemakaian Barang</li>
            </ol>   
<!-- Tampil Data Dan seacrh Native -->

        <div class="row">
            <div class="col-ig-12">
                <div class="table-responsive">
                        
                    <table class="table table-bordered table-hover table-striped" id="datatables">
                         
<!--Kepala Judul-->     <thead>
                        <tr>  
                            <th>No</th>                          
                            <th>Barang Dipakai</th> 
                            <th>Merek</th>                        
                            <th>Pemakaian</th>
                            <th>Jumlah</th>
                            <th>Tanggal</th>
                            <th>Keterangan</th>
                            <th>Opsi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                            include "../../LTA1/config/koneksi.php";
                            $brg = "SELECT * FROM tbpemakaian";

                            $query = mysqli_query($a, $brg);

                            $no = 1;
                            while($data = mysqli_fetch_array($query)){
                        ?>
                        <tr>
                            <td align="center"><?php echo $no++; ?></td>                        
                            <td><?php echo $data ['NamaBrg']; ?></td>
                            <td><?php echo $data ['Brand']; ?></td>
                            <td><?php echo $data ['Pemakaian']; ?></td>
                            <td><?php echo $data ['Jumlah']; ?></td>
                            <td><?php echo $data ['Tanggal']; ?></td>
                            <td><?php echo $data ['Keterangan']; ?></td>
                          
                            <td align="Center">
 <!--Edit Data-->                              
                            <a>                                                                               <!--       Edit data     -->
                            <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#edit<?php echo $data['No'];?>"><i class "fa fa-edit"></i>EDIT</button>                                   
                            </div>

                            <a href="../../LTA1/model/Manj/HapusPemakaianManj.php?<?php echo "No=" .$data['No']; ?>" onclick="javascript: return confirm('Anda yakin hapus ?')" class="btn btn-danger btn-xs">HAPUS</a>
                            </a>
                            </td>
                        </tr>
                         <?php
                        }
                        
                          ?> 
                        </tbody>
                    </table>
                </div>
<!-- modal start -->       
                    <?php 
                        include "../../LTA1/config/koneksi.php";
                            $brg = "SELECT * FROM tbpemakaian";
                            $query = mysqli_query($a, $brg);
                            $no = 1;
                        while($data = mysqli_fetch_array($query)){                                   
                    ?>
            
                         <div id="edit<?php echo $data['No'];?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" align="center">Edit Data Barang</h4>
                                    </div>
                                    
                                    <form action="../../LTA1/model/Manj/EditPemakaianManj.php" method="post">
                                        <div class="modal-body">                                          
                                            <div class="form-group">
                                            <label class="control-label" for="NamaBrg"> Barang </label>
                                            <input type="text" name="NamaBrg" value="<?php echo $data ['NamaBrg']; ?>" class="form-control" id="NamaBrg" required />
                                            <input type="hidden" name="No" value="<?php echo $data ['No']; ?>"> 
                                             <input type="hidden" name="ID_B" value="<?php echo $data ['ID_B']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="Brand">Brand</label>
                                            <input type="text" name="Brand" value="<?php echo $data ['Brand']; ?>" class="form-control" id="Brand" required />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="Pemakaian">Pemakaian</label>
                                            <input type="text" name="Pemakaian" value="<?php echo $data ['Pemakaian']; ?>" class="form-control" id="Pemakaian" required />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="Jumlah">Jumlah</label>
                                            <input type="number" name="JumlahPakai" value="<?php echo $data ['Jumlah']; ?>" class="form-control" id="Jumlah" required />
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label" for="Tanggal">Tanggal</label>
                                            <input type="date" name="Tanggal" value="<?php echo $data ['Tanggal']; ?>" class="form-control" id="Tanggal" required />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="Keterangan">Keterangan</label>
                                            <input type="text" name="Keterangan" value="<?php echo $data ['Keterangan']; ?>" class="form-control" id="Keterangan" required />
                                        </div>
                                      
                                     
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-danger">reset</button>
                                        <input type="submit" class="btn btn-success" value="simpan">
                                    </div>
                                    </div>

                                    </form>
                                </div>
                                </div>
                            </div>
                              <?php
                                         }
                                    ?> 
                <!-- end modal -->
 <!-- Tambah Data Native -->

                 <button type="button" class= "btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Data Pemakaian</button>
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" align="center">Tambah Data Pemakaian Barang</h4>
                        </div>
                        <form action="../../LTA1/model/Manj/m_PemakaianManj.php" method="post">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="control-label" for="NamaBrg"> Barang </label>
                          
                                      <select name="ID_B" class="form-control" id="Departement" required>
                                        <?php 
                                        include "../../LTA1/config/koneksi.php";
                                            $brg = "SELECT * FROM tbbarang";
                                            $query = mysqli_query($a, $brg);
                                            $no = 1;
                                        while($data = mysqli_fetch_array($query)){                                   
                                        ?>
                                        <option value="<?php echo $data['ID_B'] ?>"><?php echo $data['NamaBrg'] ?>&nbsp;<?php echo $data['Brand'] ?> (Sisa Stok :<?php echo $data['Jumlah'] ?>) </option>

                                         <?php } ?>
                                       </select>

                                </div>



                         

                                <div class="form-group">
                                    <label class="control-label" for="Pemakaian">Pemakai</label>
                                    <input type="text" name="Pemakaian" class="form-control" id="Pemakaian" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="Jumlah">Jumlah Di Pakai</label>
                                    <input type="number" name="JumlahPakai" class="form-control" id="Jumlah" required>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label" for="Tanggal">Tanggal</label>
                                    <input type="date" name="Tanggal" class="form-control" id="Tanggal" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="Keterangan">Keterangan</label>
                                    <input type="text" name="Keterangan" class="form-control" id="Keterangan" required>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-danger">reset</button>
                                <input type="submit" class="btn btn-success" value="simpan">
                            </div>
                        </form>
                       
                    </div>
                 </div>
              </div>
                        
            </div> 
        </div>
