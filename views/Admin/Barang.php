 
        <div class="row">
        <div class="col-lg-12">
         <h1><i class="fa fa-shopping-cart fa"></i> Barang <small>Data Barang</small></h1>
         <ol class="breadcrumb"> <li><a href="?page=dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</li></a>    
            <li><i class="fa fa-shopping-cart fa"></i>  Data Barang</li>
         </ol>
        </div>
        </div>
<!-- Tampil Data Dan seacrh Native -->

        <div class="row">
        	<div class="col-ig-12">
        		<div class="table-responsive">
        			<table class="table table-bordered table-hover table-striped" id="datatables">
<!--Kepala Judul-->   <thead>
        				<tr>  
        					<th>ID</th>
                            <th>NO Purchase Request</th>
        					<th>Nama Barang</th>
        					<th>Brand</th>
        					<th>Jenis</th>
        					<th>Jumlah</th>
        					<th>Tempat</th>
        					<th>Tanggal</th>
                            <th>Keterangan</th>
        					<th>Opsi</th>
        				</tr>
                      </thead>
                      <tbody>
                        <?php
                            include "../../LTA1/config/koneksi.php";
                            $brg = "SELECT * FROM tbbarang";

                            $query = mysqli_query($a, $brg);

                            $no = 1;
                            while($data = mysqli_fetch_array($query)){
                        ?>
                        <tr>
                            <td align="center"><?php echo $no++; ?></td>
                            <td><?php echo $data ['NO_PR']; ?></td>
                            <td><?php echo $data ['NamaBrg']; ?></td>
                            <td><?php echo $data ['Brand']; ?></td>
                            <td><?php echo $data ['Jenis']; ?></td>
                            <td><?php echo $data ['Jumlah']; ?></td>
                            <td><?php echo $data ['Tempat']; ?></td>
                            <td><?php echo $data ['Tanggal']; ?></td>
                            <td><?php echo $data ['Keterangan']; ?></td>
                          
                            <td align="Center">
 <!--Edit Data-->                              
                            <a>
                            <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#edit<?php echo $data['ID_B'];?>"><i class "fa fa-edit"></i>EDIT</button>                                   
                            </div>                               
                            
<!--Hapus Data-->
                            </a>
                            <a href="../../LTA1/model/Admin/HapusBarang.php?<?php echo "ID_B=" .$data['ID_B']; ?>" onclick="javascript: return confirm('Anda yakin hapus ?')" class="btn btn-danger btn-xs">HAPUS</a>
                            </a>
                            </td>
        				</tr>
                         <?php
                        }
                        
                          ?> 
                        </tbody>
        			</table>
        		</div>
            </div>
        </div>
                <!-- modal start -->
                <?php 
                        include "../../LTA1/config/koneksi.php";
                            $brg = "SELECT * FROM tbbarang";
                            $query = mysqli_query($a, $brg);
                            $no = 1;
                        while($data = mysqli_fetch_array($query)){                                   
                    ?>
                         <div id="edit<?php echo $data['ID_B'];?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" align="center">Edit Data Barang</h4>
                                    </div>
                                    <form action="../../LTA1/model/Admin/EditBarang.php" method="post">
                                    <div class="modal-body">                                          
                                                
                                    <div class="form-group">
                                        <label class="control-label" for="NO_PR">NO Purchase Request</label>
                                        <input type="number" name="NO_PR" value="<?php echo $data ['NO_PR']; ?>" class="form-control" id="NO_PR" required />
                                    </div>
                                    <input type="hidden" name="ID_B" value="<?php echo $data ['ID_B']; ?>">
                                              
                                        <div class="form-group">
                                        <label class="control-label" for="NamaBrg">Nama Barang</label>
                                        <input type="text" name="NamaBrg" value="<?php echo $data ['NamaBrg']; ?>"class="form-control" id="NamaBrg" required />
                                    </div>
                                           
                                    <div class="form-group">
                                        <label class="control-label" for="Brand">Brand</label>
                                        <input type="text" name="Brand" value="<?php echo $data ['Brand']; ?>"class="form-control" id="Brand" required/>
                                    </div>
                                          
                                    <div class="form-group">
                                        <label class="control-label" for="Jenis">Jenis</label>
                                        <input type="text" name="Jenis" value="<?php echo $data ['Jenis']; ?>"class="form-control" id="Jenis" required/>
                                    </div>                                         
                                        
                                    <div class="form-group">
                                        <label class="control-label" for="Jumlah">Jumlah</label>
                                        <input type="number" name="Jumlah" value="<?php echo $data ['Jumlah']; ?>"class="form-control" id="Jumlah" required/>
                                        </div>                                     
                                        
                                    <div class="form-group">
                                        <label class="control-label" for="Tempat">Tempat</label>
                                        <input type="text" name="Tempat" value="<?php echo $data ['Tempat']; ?>"class="form-control" id="Tempat" required/>
                                    </div>                                       
                                    <div class="form-group">
                                        <label class="control-label" for="Tanggal">Tanggal</label>
                                        <input type="date" name="Tanggal" value="<?php echo $data ['Tanggal']; ?>"class="form-control" id="Tanggal" required/>
                                    </div>
                                     <div class="form-group">
                                        <label class="control-label" for="Keterangan">Keterangan</label>
                                        <input type="text" name="Keterangan" value="<?php echo $data ['Keterangan']; ?>"class="form-control" id="Keterangan" required/>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-danger">reset</button>
                                        <input type="submit" class="btn btn-success" value="simpan">
                                    </div>
                                    </div>
                                    </form>
                                </div>
                                </div>
                            </div>
                            <?php
                                }
                            ?>
                <!-- end modal -->
                

 <!-- Tambah Data Native -->

                 <button type="button" class= "btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Data</button>
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" align="center">Tambah Data Barang</h4>
                        </div>
                        <form action="../../LTA1/model/Admin/m_barang.php" method="post">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="control-label" for="NO_PR"> No Purchase Request</label>
                                    <input type="number" name="NO_PR" class="form-control" id="NO_PR" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="NamaBrg">Nama Barang</label>
                                    <input type="text" name="NamaBrg" class="form-control" id="NamaBrg" required>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label" for="Brand">Brand</label>
                                    <input type="text" name="Brand" class="form-control" id="Brand" required>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label" for="Jenis">Jenis</label>
                                    <input type="text" name="Jenis" class="form-control" id="Jenis" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="Jumlah">Jumlah</label>
                                    <input type="number" name="Jumlah" class="form-control" id="Jumlah" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="Tempat">Tempat</label>
                                    <input type="text" name="Tempat" class="form-control" id="Tempat" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="Tanggal">Tanggal Datang</label>
                                    <input type="date" name="Tanggal" class="form-control" id="Tanggal" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="Keterangan">Keterangan</label>
                                    <input type="text" name="Keterangan" class="form-control" id="Keterangan" required>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-danger">reset</button>
                                <input type="submit" class="btn btn-success" value="simpan">
                            </div>
                        </form>
                    </div>
                 </div>
              </div>
                </div>     
        	</div> 
        </div>