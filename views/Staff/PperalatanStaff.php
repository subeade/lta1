
<!-- Tampil Data Native -->
        <div class="row">
          <div class="col-lg-12">
            <h1><i class="fa fa-fw fa-wrench"></i> Peminjam Peralatan</h1>
            <ol class="breadcrumb">
               <a href="?page=dashboard"><i class="fa fa-fw fa-dashboard"></i>Dashboard</a></li>
                <li><i class="fa fa-fw fa-wrench"></i>  Peminjaman Peralatan</li> 
                <!--Tombol Print-->
                <button><a style="font-size:15px"href="?page=Print"> Print <i class="fa fa-print"></a></i></button>
            
            </ol>
          </div>
        </div><!-- /.row -->
        <div class="row">
        	<div class="col-ig-12">
        		<div class="table-responsive">
        			<table class="table table-bordered table-hover table-striped" id="datatables">
        			 <thead>
                        <tr>  
        					<th>ID</th>
        					 
                            <th align="center">Alat</th>
        					<th align="center">Nama </th>
        					<th align="center">Departement</th>        					
        					<th align="center">Di Pinjam Tanggal</th>
        					<th align="center">Di Kembalikan Tanggal</th>
        					<th align="center">Keterangan</th>
        					<th align="center">Status</th>
        				</tr>
                     </thead>
                     <tbody>
                       <?php
                            include "../../LTA1/config/koneksi.php";
                            $brg = "SELECT * FROM tbPperalatan";

                            $query = mysqli_query($a, $brg);

                            $no = 1;
                            while($data = mysqli_fetch_array($query)){
                        ?>
                        <tr>
                            <td align="center"><?php echo $no++; ?></td>

                            <td><?php echo $data ['Alat']; ?></td>
                            <td><?php echo $data ['NamaP']; ?></td>
                            <td><?php echo $data ['Departement']; ?></td>
                            <td><?php echo $data ['DP_Tanggal']; ?></td>
                            <td><?php echo $data ['DK_Tanggal']; ?></td>
                            <td><?php echo $data ['Keterangan']; ?></td>
                            <td class="row">
                                <div class="col-xs-8">
                                    <?php
                                    if ($data['Status']==1) {
                                        echo "<a href='../../LTA1/model/Staff/hapusstatuspeminjamStaff.php?ID_P=".$data['ID_P']."' title='Hapus Status'><i class='fa fa-check'></i></a>";
                                        echo "</br>";
                                        echo "Sudah dikembalikan";
                                    }else{
                                        echo " <a href='../../LTA1/model/Staff/setstatusbarang.php?ID_P=".$data['ID_P']."' title='Set status barang'><i class='fa fa-close'></i></a>";
                                        echo "</br>";
                                        echo "Belum dikembalikan";
                                    }
                                    ?>

                                 
                                  </div>
                            </td>
        				</tr>
                        <?php
                        }
                        ?>	
                    </tbody>
        			</table>
        		</div>

<!-- Tambah Data Native -->

                 <button type="button" class= "btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Data</button>
              <div id="tambah" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" align="center">Tambah Data Peminjam</h4>
                        </div>
                        <form action="../../LTA1/model/Staff/m_PperalatanStaff.php" method="post">
                            <div class="modal-body"> 
                                <div class="form-group">
                                    <label class="control-label" for="Alat">Alat</label>
                                    <input type="text" name="Alat" class="form-control" id="Alat" required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="NamaP">Nama Peminjam</label>
                                    <input type="text" name="NamaP" class="form-control" id="NamaP" required>
                                </div>
                                
                                    <label style="margin-left: 5px;" for="Departement">Departement</label>
                                    <div class="form-group">
                                        <select name="Departement" class="form-control" id="Departement" required>
                                        <option value="">- PILIH -</option>
                                        <option value="FB Sercice">FB Service</option>
                                        <option value="Engenering">Engenering</option>
                                        <option value="HRD">HRD</option>
                                        <option value="Housekeeping">House Keeping</option>
                                        <option value="sales">Sales</option>
                                        </select>
                                    </div>    

                                <div class="form-group">
                                    <label class="control-label" for="DP_Tanggal">Di Pinjam Tanggal</label>
                                    <input type="date" name="DP_Tanggal" class="form-control" id="DP_Tanggal" required>                                 
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="DK_Tanggal">Di Kembalikan Tanggal</label>
                                    <input type="date" name="DK_Tanggal" class="form-control" id="DK_Tanggal">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="Keterangan">Keterangan</label>
                                    <input type="text" name="Keterangan" class="form-control" id="Keterangan" required>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-danger">reset</button>
                                <input type="submit" class="btn btn-success" name="tambah" value="simpan">
                            </div>
                        </form>
                    
                    </div>
                 </div>
              </div>
        	</div> 
        </div>