
  
  <div class="col-lg-12">
    <h1><i class="fa fa-fw fa-user-md "></i>PROFILE <small><?php echo $_SESSION['User'];?></small></h1>
    <ol class="breadcrumb">
    <li><a href="?page=dashboard"><i class="fa fa-fw fa-dashboard"></i>Dashboard</a></li>
    </ol>
  </div>

<!--Masuk ke assets-->     
  <link rel="stylesheet" href="../assets/bower_components/Ionicons/css/ionicons.min.css">
<!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../assets/dist/css/skins/_all-skins.min.css">
 
<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

<!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="../assets/img/GMR.jpg" alt="User profile picture">

              <h3 class="profile-username text-center"></h3>

              <p class="text-muted text-center">DEPARTEMENT IT </p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>USER</b> <a class="pull-right">IT&nbsp;<?php echo $_SESSION['User'];?></a>
                </li>
                <li class="list-group-item">
                  <b>JABATAN</b> <a class="pull-right">IT&nbsp;<?php echo $_SESSION['User'];?></a>
                </li>
                <li class="list-group-item">
                  <b>Email</b> <a class="pull-right">Sisteminventory321@gmail</a>
                </li>
              </ul>

              <a href="?page=Email" class="btn btn-primary btn-block"><b>Email</b></a>
              <a href="../Model/Logout.php "onclick="javascript: return confirm('Log Out')" class="btn btn-danger btn-block">Logout</a>
            </div>
          </div>
          </div>
        </div>
<!-- /.col -->
       
  <div class="control-sidebar-bg"></div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../assets/dist/js/demo.js"></script>
</body>
</html>
