
<!-- Data Dengan Sistem OOP -->
<?php
include('../config/koneksi.php');
  session_start();
    if(!isset($_SESSION['User'])){
      die("Anda belum login");

  } 
    if($_SESSION['Level']!="Admin"){
      die("Anda Bukan Admin");
  }
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aplikasi Monitoring Inventory</title>
    <link rel="stylesheet" href="../assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../assets/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../assets/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- admin lte css -->
<!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
<!-- JS Dan CSS DataTables-->

    <link href="../assets/dataTables/datatables.min.css" rel="stylesheet">
    <link href="../assets/dataTables/datatables.min.css" rel="stylesheet">
    
<!-- Add custom CSS here -->
    <link href="../assets/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
<!-- Add custom CSS here -->
    <link href="../assets/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="">Aplikasi Monitoring Inventory</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
                <li><a><h3><i class="fa fa-fw fa-user-md "></i> ADMIN IT</h3></a></li>
                <li><a href="?page=dashboard"><i class="fa fa-dashboard"></i>  Dashboard</a></li>
                <li><a href="?page=Email"><i class="fa fa-envelope"></i> Email</a></li>
                <li><a href="?page=Barang"><i class="fa fa-shopping-cart fa"></i>  Data Barang</a></li>
                <li><a href="?page=DataPemakaian"><i class="ion ion-bag"></i> Data Pemakaian Barang</a></li>
                <li><a href="?page=Pperalatan"><i class="fa fa-fw fa-wrench"></i>  Peminjam Peralatan</a></li>
                <li><a href="?page=Print"><i class="fa fa-print"></i> Print Peminjam Barang</a></li>
               
                
              </ul>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user">&nbsp;</i><?php echo $_SESSION['User'];?> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="?page=profile"><i class="fa fa-user"></i> Profile</a></li>
                <li class="divider"></li>
                <li><a href="../model/Logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper">
<?php
    if (@$_GET['page'] == 'dashboard' || @$_GET['page'] == '') {
    include "Admin/dashboard.php";
  } if (@$_GET['page'] == 'Barang') {
    include "Admin/Barang.php";
  } if (@$_GET['page'] == 'Pperalatan') {
    include "Admin/Pperalatan.php";
  } if (@$_GET['page'] == 'DataPemakaian') {
    include "Admin/DataPemakaian.php";
  } if (@$_GET['page'] == 'Email'){
    include "Admin/Email.php"; 
  } if (@$_GET['page'] == 'profile') {
    include "profile.php";
  } else if (@$_GET['page'] == 'Print') {
    include "Admin/Print.php";
  } 
?>
      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
    <script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/bootstrap.js"></script>

<!--Java Script DataTables-->    

    <script src="../assets/dataTables/datatables.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
       $('#datatables').DataTable();   
      });
    </script>

  </body>
</html>